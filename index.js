var clipboard = require('sdk/clipboard');
var contextMenu = require('sdk/context-menu');
var request = require('sdk/request');
var self = require('sdk/self');
var url = require('sdk/url');

contextMenu.addItem(contextMenu.Item({
  label: 'Stollaksel',
  contentScriptFile: self.data.url('click.js'),
  onMessage: function (data) {
    request.Request({
      // TODO make the endpoint a preference of the add-on
      url: 'https://stollaksel.ewie.name/url',
      content: url.URL(data.target, data.base).toString(),
      contentType: 'text/plain',
      headers: {
        'accept': 'text/plain'
      },
      onComplete: function (response) {
        clipboard.set(response.headers['Location']);
      }
    }).post();
  }
}));
