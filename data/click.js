var TARGET_URL_ATTR_NAMES = {
  a: 'href',
  img: 'src'
};

function getOwn(obj, p) {
  if (obj.hasOwnProperty(p)) return obj[p];
}

self.on('click', function (node) {
  var attrName = getOwn(TARGET_URL_ATTR_NAMES, node.tagName.toLowerCase());
  var baseUrl = node.ownerDocument.location.href;
  self.postMessage({
    target: attrName ? node.getAttribute(attrName) : baseUrl,
    base: baseUrl
  });
});
